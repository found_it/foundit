import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:found_it/Main Page/sign_in.dart';
import 'package:flutter/material.dart';
import 'package:found_it/model/user.dart';

class SignUpPage extends StatefulWidget {


  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  FirebaseDatabase database = FirebaseDatabase.instance;
  DatabaseReference databaseReference;
  user User;

  @override
  void initState() {
    super.initState();

    databaseReference = database.reference().child("users");
  }

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String _email, _password,fname,lastname,phone;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Form(
          key: _formKey,
      child: SignupPage()
          ),
//      appBar: new AppBar(),
//      body: Form(
//          key: _formKey,
//          child: Column(
//            children: <Widget>[
//              TextFormField(
//                validator: (input) {
//                  if(input.isEmpty){
//                    return 'Provide an email';
//                  }
//                },
//                decoration: InputDecoration(
//                    labelText: 'Email'
//                ),
//                onSaved: (input) => _email = input,
//              ),
//              TextFormField(
//                validator: (input) {
//                  if(input.length < 6){
//                    return 'Longer password please';
//                  }
//                },
//                decoration: InputDecoration(
//                    labelText: 'Password'
//                ),
//                onSaved: (input) => _password = input,
//                obscureText: true,
//              ),
//              RaisedButton(
//                onPressed: signUp,
//                child: Text('Sign up'),
//              ),
//            ],
//          )
//      ),
    );
  }

  Widget SignupPage() {
    return new Container(
      height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          color: Colors.teal,
          image: DecorationImage(
            colorFilter: new ColorFilter.mode(
                Colors.black.withOpacity(0.3), BlendMode.dstATop),
            image: AssetImage('assets/images/backg.png'),
            fit: BoxFit.cover,
          ),
        ),
      child: SingleChildScrollView(
      child: new Column(
        children: <Widget>[
          Container(
            padding: const EdgeInsets.only(left: 120.0, right: 120.0, top: 100.0, bottom: 30.0),
            child: Center(
              child: Icon(
                Icons.search,
                color: Colors.white,
                size: 150.0,
              ),
            ),
          ),
//          new Row(
//            children: <Widget>[
//              new Expanded(
//                child: new Padding(
//                  padding: const EdgeInsets.only(left: 40.0),
//                  child: new Text(
//                    "EMAIL",
//                    style: TextStyle(
//                      fontWeight: FontWeight.bold,
//                      color: Colors.white,
//                      fontSize: 15.0,
//                    ),
//                  ),
//                ),
//              ),
//            ],
//          ),
          new Container(
            width: MediaQuery.of(context).size.width,
            margin: const EdgeInsets.only(left: 40.0, right: 40.0),
            alignment: Alignment.center,
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(
                    color: Colors.white,
                    width: 0.5,
                    style: BorderStyle.solid),
              ),
            ),
            child: new Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                new Expanded(
                  child: TextFormField(
                    style: new TextStyle(color: Colors.white),
                    decoration: InputDecoration(
                      labelText: "EMAIL",
                      labelStyle: TextStyle(color: Colors.white),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                      ),
                    ),
                validator: (input) {
                  if(input.isEmpty){
                    return 'Provide an email';
                  }
                },
                onSaved: (input) => _email = input,
              ),
                ),
              ],
            ),
          ),
          Divider(
            height: 10.0,
          ),
//          new Row(
//            children: <Widget>[
//              new Expanded(
//                child: new Padding(
//                  padding: const EdgeInsets.only(left: 40.0),
//                  child: new Text(
//                    "PASSWORD",
//                    style: TextStyle(
//                      fontWeight: FontWeight.bold,
//                      color: Colors.white,
//                      fontSize: 15.0,
//                    ),
//                  ),
//                ),
//              ),
//            ],
//          ),x
          new Container(
            width: MediaQuery.of(context).size.width,
            margin: const EdgeInsets.only(left: 40.0, right: 40.0),
            alignment: Alignment.center,
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(
                    color: Colors.white,
                    width: 0.5,
                    style: BorderStyle.solid),
              ),
            ),
            child: new Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                new Expanded(
                  child: TextFormField(
                    style: new TextStyle(color: Colors.white),
                    decoration: InputDecoration(
                      labelText: "PASSWORD",
                      labelStyle: TextStyle(color: Colors.white),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                      ),
                    ),
                validator: (input) {
                  if(input.length < 6){
                    return 'Longer password please';
                  }
                },
                onSaved: (input) => _password = input,
                obscureText: true,
              ),
                ),
              ],
            ),
          ),
          Divider(
            height: 10.0,
          ),
//          new Row(
//            children: <Widget>[
//              new Expanded(
//                child: new Padding(
//                  padding: const EdgeInsets.only(left: 40.0),
//                  child: new Text(
//                    "CONFIRM PASSWORD",
//                    style: TextStyle(
//                      fontWeight: FontWeight.bold,
//                      color: Colors.white,
//                      fontSize: 15.0,
//                    ),
//                  ),
//                ),
//              ),
//            ],
//          ),
          new Container(
            width: MediaQuery.of(context).size.width,
            margin: const EdgeInsets.only(left: 40.0, right: 40.0),
            alignment: Alignment.center,
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(
                    color: Colors.white,
                    width: 0.5,
                    style: BorderStyle.solid),
              ),
            ),
            child: new Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                new Expanded(
                  child: TextFormField(
                    style: new TextStyle(color: Colors.white),
                    decoration: InputDecoration(
                      labelText: "CONFIRM PASSWORD",
                      labelStyle: TextStyle(color: Colors.white),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                      ),
                    ),
                    obscureText: true,
                    textAlign: TextAlign.left,

                  ),
                ),
              ],
            ),
          ),
          Divider(
            height: 10.0,
          ),

          new Container(
            width: MediaQuery.of(context).size.width,
            margin: const EdgeInsets.only(left: 40.0, right: 40.0),
            alignment: Alignment.center,
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(
                    color: Colors.white,
                    width: 0.5,
                    style: BorderStyle.solid),
              ),
            ),
            child: new Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                new Expanded(
                  child: TextFormField(
                    style: new TextStyle(color: Colors.white),

                    decoration: InputDecoration(
                      labelText: "FIRST NAME",
                      labelStyle: TextStyle(color: Colors.white),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                      ),

                    ),

                    textAlign: TextAlign.left,
                    onSaved: (input) => fname = input,

                  ),

                ),

              ],
            ),
          ),

          Divider(
            height: 10.0,
          ),

          new Container(
            width: MediaQuery.of(context).size.width,
            margin: const EdgeInsets.only(left: 40.0, right: 40.0),
            alignment: Alignment.center,
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(
                    color: Colors.white,
                    width: 0.5,
                    style: BorderStyle.solid),
              ),
            ),
            child: new Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                new Expanded(
                  child: TextFormField(
                    style: new TextStyle(color: Colors.white),
                    decoration: InputDecoration(
                      labelText: "LAST NAME",
                      labelStyle: TextStyle(color: Colors.white),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                      ),
                    ),
                    textAlign: TextAlign.left,
                    onSaved: (input) => lastname = input,

                  ),
                ),
              ],
            ),
          ),

          Divider(
            height: 10.0,
          ),

          new Container(
            width: MediaQuery.of(context).size.width,
            margin: const EdgeInsets.only(left: 40.0, right: 40.0),
            alignment: Alignment.center,
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(
                    color: Colors.white,
                    width: 0.5,
                    style: BorderStyle.solid),
              ),
            ),
            child: new Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                new Expanded(
                  child: TextFormField(
                    style: new TextStyle(color: Colors.white),
                    decoration: InputDecoration(
                      labelText: "PHONE NUMBER",
                      labelStyle: TextStyle(color: Colors.white),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                      ),
                    ),
                    textAlign: TextAlign.left,
                    keyboardType: TextInputType.number,
                    onSaved: (input) => phone = input,

                  ),
                ),
              ],
            ),
          ),

          Divider(
            height: 15.0,
          ),



          new Container(
            margin: const EdgeInsets.only(left: 40.0),
            alignment: AlignmentDirectional(-1.0, 0.0),
            child: RaisedButton(
            child: Text("UPLOAD ID"),
            textColor: Colors.teal,
            color: Colors.white,
            onPressed: () {
              Fluttertoast.showToast(
                  msg: "Soon!",
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.CENTER,
                  timeInSecForIos: 1,
                  backgroundColor: Colors.red,
                  textColor: Colors.white,
                  fontSize: 16.0);
              },
            ),
          ),

          new Container(
            margin: const EdgeInsets.only(left: 40.0),
            alignment: AlignmentDirectional(-1.0, 0.0),
            child: Text('No Image Uploaded', style: TextStyle(color: Colors.white),),
          ),

          new Container(
            width: MediaQuery.of(context).size.width,
            margin: const EdgeInsets.only(left: 30.0, right: 30.0, top: 50.0, bottom: 20.0),
            alignment: Alignment.center,
            child: new Row(
              children: <Widget>[
                new Expanded(
                  child: new FlatButton(
                    shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(30.0),
                    ),
                    color: Colors.white,
                    onPressed: signUp,
                    child: new Container(
                      padding: const EdgeInsets.symmetric(
                        vertical: 20.0,
                        horizontal: 20.0,
                      ),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new Expanded(
                            child: Text(
                              "SIGN UP",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.teal,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
      )
    );
  }


  void signUp() async {
    if(_formKey.currentState.validate()){
      _formKey.currentState.save();
      try{
        FirebaseUser users =  await FirebaseAuth.instance.createUserWithEmailAndPassword(email: _email, password: _password);
        User=user(users.uid,fname, lastname, _email,  phone, "https://firebasestorage.googleapis.com/v0/b/found-it-6a3f6.appspot.com/o/home-icon-png-transparent.png?alt=media&token=beb9ebc9-029c-4a26-9278-f6591ea97704");
        databaseReference.child(User.key).set(User.toJson());

        users.sendEmailVerification();
        Navigator.push(context, MaterialPageRoute(builder: (context) => LoginPage(), fullscreenDialog: false));
      }catch(e){
        print(e.message);
        Fluttertoast.showToast(
            msg: e.message,
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 1,
            backgroundColor: Colors.red,

            textColor: Colors.white,
            fontSize: 16.0
        );
      }
    }
  }
}