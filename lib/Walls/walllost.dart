import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:found_it/CardPress.dart';

import 'package:found_it/model/board.dart';


import 'package:firebase_database/firebase_database.dart';
import 'package:fluttertoast/fluttertoast.dart';

class MyHomePageLost extends StatefulWidget {
  @override
  wall createState() => new wall();
}

class wall extends State<MyHomePageLost> {
  List<Board> boardMessages = List();
  Board board;
  final FirebaseDatabase database = FirebaseDatabase.instance;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  DatabaseReference databaseReference;

  @override
  void initState() {
    super.initState();

    board = Board("", "","","","","");
    databaseReference = database.reference().child("Lost Items");
    databaseReference.onChildAdded.listen(_onEntryAdded);
    databaseReference.onChildChanged.listen(_onEntryChanged);
  } //  void _incrementCounter() {
////    database.reference().child("messge").push().set({
////       "firstname" : "iOS",
////      "lastname" : "X",
////      "age" : 1
////    });
//
//    database
//        .reference()
//        .child("message")
//        .set({"firstname": "iOS", "Lastname": "X", "Age": 1});
//
//    setState(() {
//      database
//          .reference()
//          .child("message")
//          .once()
//          .then((DataSnapshot snapshot) {
//        Map<dynamic, dynamic> data = snapshot.value;
//
//        print("Values from db: ${snapshot.value}");
//      });
//
//      _counter++; 
//
//    });
//  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor:  Colors.white,
      body: Column(
        children: <Widget>[
//          Flexible(
//            flex: 0,
//            child: Center(
//              child: Form(
//                key: formKey,
//                child: Flex(
//                  direction: Axis.vertical,
//                  children: <Widget>[
//
//                    ListTile(
//                      leading: Icon(
//                          Icons.subject,
//                      ),
//                      title: TextFormField(
//                        decoration: InputDecoration(
//                            labelText: 'Subject'
//                        ),
//                        initialValue: "",
//                        onSaved: (val) => board.subject = val,
//                        validator: (val) => val == "" ? val : null,
//                      ),
//                    ),
//
//                    ListTile(
//                      leading: Icon(Icons.message),
//                      title: TextFormField(
//                        decoration: InputDecoration(
//                            labelText: 'Message'
//                        ),
//                        initialValue: "",
//                        onSaved: (val) => board.body = val,
//                        validator: (val) => val == "" ? val : null,
//                      ),
//                    ),
//
//                    //Send or Post button
//                    FlatButton(
//                      child: Text("Post"),
//                      textColor: Color(0xFFFFFFFF),
//                      color: Colors.teal,
//                      onPressed: () {
//                        handleSubmit();
//                      },
//                    )
//                  ],
//                ),
//              ),
//            ),
//          ),
          Flexible(
            child: FirebaseAnimatedList(
              query: databaseReference,
              itemBuilder: (_, DataSnapshot snapshot,
                  Animation<double> animation, int index) {
                return new GestureDetector(
                  onTap: () {
                    add();
//                    Fluttertoast.showToast(
//                        msg: "Hehee lost boi!",
//                        toastLength: Toast.LENGTH_SHORT,
//                        gravity: ToastGravity.CENTER,
//                        timeInSecForIos: 1,
//                        backgroundColor: Colors.red,
//                        textColor: Colors.white,
//                        fontSize: 16.0
//                    );
                  },
                  child: Card(

//                    elevation: 30.0,
                      margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
                      child: Container(
                        color: Colors.teal,
                        child: ListTile(
                          leading: CircleAvatar(
                            radius: 25.0,
                            backgroundColor: Colors.transparent,
                            backgroundImage: new NetworkImage('https://firebasestorage.googleapis.com/v0/b/found-it-6a3f6.appspot.com/o/Capture.PNG?alt=media&token=da30cf6b-1177-4d38-9770-2c351193e57c',
                            ),
                          ),
                          title: new Text(
                            boardMessages[index].subject,
                            style: TextStyle(color: Colors.white, fontSize: 25),

                          ),
                          subtitle: new Text(
                            boardMessages[index].body,
                            style: TextStyle(color: Colors.white, fontStyle: FontStyle.italic, fontSize: 11),
                          ),
                        ),
                      )

                  )
                );
              },
            ),
          ),
//
//      FloatingActionButton(
//      onPressed:  () {
//        Navigator.push(context, MaterialPageRoute(builder: (context) => CreatePost(), fullscreenDialog: true));
//
//      },
//      tooltip: 'Add Image',
//      child: new Icon(Icons.add),
//    ),
        ],
      ),
    );
  }
//
  void _onEntryAdded(Event event) {
    setState(() {
      boardMessages.add(Board.fromSnapshot(event.snapshot));
    });
  }

  void handleSubmit() {
    final FormState form = formKey.currentState;
    if (form.validate()) {
      form.save();
      form.reset();

      //save form data to the database
      databaseReference.push().set(board.toJson());
    }
  }

  void _onEntryChanged(Event event) {
    var oldEntry = boardMessages.singleWhere((entry) {
      return entry.key == event.snapshot.key;
    });

    setState(() {
      boardMessages[boardMessages.indexOf(oldEntry)] =
          Board.fromSnapshot(event.snapshot);
    });
  }
  add (){
  //  Navigator.push(context, MaterialPageRoute(builder: (context) =>  CardPost())); //Home(user: user)


  }
}
