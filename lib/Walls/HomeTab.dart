import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:found_it/CreatePost.dart';
import 'package:found_it/Walls/wall.dart';
import 'package:found_it/Walls/walllost.dart';
import 'package:found_it/model/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../CreatePostLost.dart';

class TabHome extends StatefulWidget {

  @override
  MyApp createState() => new MyApp();

}
//

class MyApp extends State<TabHome> {

  @override
  Widget build(BuildContext context) {
    getData();
    return MaterialApp(
      home: DefaultTabController(

        length: 2,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Color(0xFF009688),
            bottom: TabBar(

              tabs: [
                Tab(text: "Found Items"),
                Tab(text: "Lost Items")
              ],
            ),
            title: Text('Lost and Found Items'),
            actions: <Widget>[
              new IconButton(
                icon: new Icon(Icons.add), tooltip: 'Air it', onPressed: _askedToLead ,),
            ],
          ),
          body: TabBarView(
            children: [
              MyHomePage(),
              MyHomePageLost(),

            ],
          ),
        ),
      ),
    );
  }

//  Future <void> add(BuildContext covariant) async {
////    Navigator.push(context, MaterialPageRoute(builder: (context) =>  MyPost())); //Home(user: user)
//
//    switch (
//    await showDialog(
//        context: context,
//        child: new SimpleDialog(
//          title: new Text("Which category?"),
//          children: <Widget>[
//            new SimpleDialogOption(
//              child: new Text("Found"),
//              onPressed: () {
//                Navigator.pop(covariant, "Found");
//                goToCreate();
//              },
//            ),
//            new SimpleDialogOption(child: new Text("Lost"),
//              onPressed: () {
//                Fluttertoast.showToast(
//                    msg: "Lost!",
//                    toastLength: Toast.LENGTH_SHORT,
//                    gravity: ToastGravity.CENTER,
//                    timeInSecForIos: 1,
//                    backgroundColor: Colors.red,
//                    textColor: Colors.white,
//                    fontSize: 16.0);
//              },
//            ),
//          ],
//        )
//    )
//    ) {
//      case "Found":
//        goToCreate();
//        break;
//
//      case "Lost":
//        Fluttertoast.showToast(
//            msg: "Lost!",
//            toastLength: Toast.LENGTH_SHORT,
//            gravity: ToastGravity.CENTER,
//            timeInSecForIos: 1,
//            backgroundColor: Colors.red,
//            textColor: Colors.white,
//            fontSize: 16.0);
//        break;
//    }
//  }
  Future<void> _askedToLead() async {
    switch (await showDialog<String>(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            title: const Text('Select Post'),
            children: <Widget>[
              SimpleDialogOption(
                onPressed: () { Navigator.pop(context, "Found");
                Navigator.push(context, MaterialPageRoute(builder: (context) =>  MyPost())); //Home(user: user)
                },
                child: const Text('Found'),
              ),
              SimpleDialogOption(
                onPressed: () { Navigator.pop(context, "Lost");
                Navigator.push(context, MaterialPageRoute(builder: (context) =>  MyPostLost())); //Home(user: user)
                },
                child: const Text('Lost'),
              ),
            ],
          );
        }
    )) {
      case "Found":
      // Let's go.
      // ...
        break;
      case "Lost":
      // ...
        break;
    }
  }
  goToCreate()
  {

    Navigator.push(context, MaterialPageRoute(builder: (context) =>  MyPost())); //Home(user: user)

  }

  getData() async{

    user u;
    SharedPreferences prefs = await SharedPreferences.getInstance();

    FirebaseUser CurrentUser= await FirebaseAuth.instance.currentUser();
    FirebaseDatabase database = FirebaseDatabase.instance;
    DatabaseReference databaseReference = database.reference().child("users");
    databaseReference.child(CurrentUser.uid).once().then((DataSnapshot snapshot) {
      print(snapshot.value.toString());
      u=user.fromSnapshot(snapshot);
    });
    prefs.setString("fname", u.getFName());
    prefs.setString("lname",u.getLName());
    prefs.setString("email",u.getEmail());
    prefs.setString("phone", u.getPhone());
    prefs.setString("imgURL",u.getIMG());
    print("hhhhhhhhhhhhhhh "+prefs.get("fname"));


  }
}