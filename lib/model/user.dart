import 'package:firebase_database/firebase_database.dart';


class user {
  String key;
  String fname;
  String lasname;
  String email;
  String phone;
  String imgURL;

  user(this.key,this.fname, this.lasname,this.email,this.phone,this.imgURL);

  user.fromSnapshot(DataSnapshot snapshot) :
        key = snapshot.key,
        fname = snapshot.value["fname"],
        lasname = snapshot.value["lasname"],
        email=snapshot.value["email"],
        phone=snapshot.value["phone"],

       imgURL=snapshot.value["imgURL"];

  String getFName(){
    return this.fname;
  }
  String getLName(){
    return this.lasname;
  }
  String getIMG(){
    return this.imgURL;
  }
  String getPhone(){
    return this.phone;
  }
  String getEmail(){
    return this.email;
  }

  toJson() {
    return {

        "fname": fname,
        "lasname": lasname,
        "email": email,
        "phone": phone,
        "imgURL": imgURL,

    };
  }



}