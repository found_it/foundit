import 'package:firebase_database/firebase_database.dart';


class Board {
   String key;
   String subject;
   String body;
   String prize;
   String imgURL;
   String userID;
   String loc;



   Board(this.subject, this.body,this.prize,this.imgURL,this.userID,this.loc);

   Board.fromSnapshot(DataSnapshot snapshot) :
          userID=snapshot.value["userID"],
          loc=snapshot.value["loc"],
          key = snapshot.key,
          subject = snapshot.value["subject"],
          body = snapshot.value["body"],
          prize=snapshot.value["prize"],
          imgURL=snapshot.value["imgURL"];




   toJson() {
      return {
         "subject": subject,
         "body" : body,
         "prize" : prize,
         "imgURL" : imgURL,
         "userID" : userID,
         "loc" : loc

      };
   }



}