import 'package:flutter/material.dart';
import 'package:found_it/CurvedNavigationBar.dart';
import 'package:found_it/Home%20Navigation/Settings.dart';
import 'package:found_it/ProfilePage.dart';
import 'package:found_it/Walls/HomeTab.dart';
import 'package:found_it/Walls/wall.dart';
import 'package:found_it/Walls/walllost.dart';

class Navigation extends StatefulWidget {
  @override
  MyApp createState() => new MyApp();

}
//
final int _pageCount =3;
int _pageIndex = 0;
class MyApp extends State<Navigation> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
    body: _body(),
    bottomNavigationBar: _bottomNavigationBar(),
    );

  }

  Widget _page(int index) {
    switch (index) {
      case 0:
        return TabHome();
      case 1:
        return ProfilePage();


      case 2:
        return MySettings();

    }

    throw "Invalid index $index";
  }

//return Scaffold(
//      bottomNavigationBar :  CurvedNavigationBar(
//        backgroundColor: Colors.teal,
//        items: <Widget>[
//          Icon(Icons.add, size: 30),
//          Icon(Icons.supervised_user_circle, size: 30),
//          Icon(Icons.list, size: 30),
//        ],
//        onTap: (index) {
//          setState(() {
//            _pageIndex = index;
//          });
//
//        },
//      ),
//
//      body: Container(color: Colors.teal),
//
//    );
CurvedNavigationBar _bottomNavigationBar() {

  return new CurvedNavigationBar(

    backgroundColor: Colors.teal,
    items: <Widget>[
      Icon(Icons.home, size: 30),
      Icon(Icons.supervised_user_circle, size: 30),
      Icon(Icons.list, size: 30),
        ],
    onTap: (int index) {
      setState(() {
        _pageIndex = index;
      });
    },
  );
}


Widget _body() {
  return Stack(
    children: List<Widget>.generate(_pageCount, (int index) {
      return IgnorePointer(
        ignoring: index != _pageIndex,
        child: Opacity(
          opacity: _pageIndex == index ? 1.0 : 0.0,
          child: Navigator(
            onGenerateRoute: (RouteSettings settings) {
              return new MaterialPageRoute(
                builder: (_) => _page(index),
                settings: settings,
              );
            },
          ),
        ),
      );
    }),
  );
}
}
