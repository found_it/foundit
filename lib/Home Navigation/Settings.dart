import 'package:flutter/material.dart';

class MySettings extends StatefulWidget {
  @override
  Settings createState() => new Settings();
}

class Settings extends State<MySettings> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor:  Colors.teal,
      appBar: AppBar(
        backgroundColor: Colors.teal,

        title: Text('Settings'),
      ),
      body: Column(
        children: <Widget>[
          Flexible(
            flex: 0,
            child: Center(
              child: Form(
                child: Flex(
                  direction: Axis.vertical,
                  children: <Widget>[
//                    Divider(
//                      height: 100.0,
//                    ),
                    new Container(
                        child: new ListTile(
                            leading: Icon(Icons.notifications_active, size: 35.0 , color: Colors.white,),
                            title: Text("Notifications",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20.0
                              ),
                            )
                        ),
                        decoration:
                        new BoxDecoration(
                            border: new Border(
                              bottom: new BorderSide(color: Colors.white),
//                              top: new BorderSide(color: Colors.white),
                            )
                        )
                    ),
                       new Container(
                          child: new ListTile(
                              leading: Icon(Icons.link, size: 35.0 , color: Colors.white,),
                              title: Text("Link social media",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 20.0
                                ),
                              )
                       ),
                           decoration:
                           new BoxDecoration(
                               border: new Border(
                                   bottom: new BorderSide(color: Colors.white),
                               )
                           )
                    ),

                    new Container(
                        child: new ListTile(
                            leading: Icon(Icons.bug_report, size: 35.0 , color: Colors.white,),
                            title: Text("Report a bug",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20.0
                              ),
                            )
                        ),
                        decoration:
                        new BoxDecoration(
                            border: new Border(
                              bottom: new BorderSide(color: Colors.white),
                            )
                        )
                    ),

                    new Container(
                        child: new ListTile(
                            leading: Icon(Icons.accessibility_new, size: 35.0 , color: Colors.white,),
                            title: Text("Tell a friend",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20.0
                              ),
                            )
                        ),
                        decoration:
                        new BoxDecoration(
                            border: new Border(
                              bottom: new BorderSide(color: Colors.white),
                            )
                        )
                    ),
                    new Container(
                        child: new ListTile(
                            leading: Icon(Icons.help, size: 35.0 , color: Colors.white,),
                            title: Text("Help",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20.0
                              ),
                            )
                        ),
                        decoration:
                        new BoxDecoration(
                            border: new Border(
                              bottom: new BorderSide(color: Colors.white),
                            )
                        )
                    ),
                    new Container(
                        child: new ListTile(
                            leading: Icon(Icons.highlight_off, size: 35.0 , color: Colors.white,),
                            title: Text("Logout",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20.0
                              ),
                            )
                        ),
                        decoration:
                        new BoxDecoration(
                            border: new Border(
                              bottom: new BorderSide(color: Colors.white),
                            )
                        )
                    ),
                  ],
                ),
              ),
            ),
          ),


        ],
      ),
    );
  }
//


}
