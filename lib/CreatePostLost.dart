import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:google_places_picker/google_places_picker.dart';

import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:found_it/model/board.dart';
import 'package:found_it/Main Page/sign_in.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:random_string/random_string.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'dart:async';
import 'package:location/location.dart';
import 'package:random_string/random_string.dart' as random;

var location = new Location();

Map<String, double> userLocation;

class MyPostLost extends StatefulWidget {



  @override
  Create createState() => new Create();

}

class Create extends State<MyPostLost> {
//  final cryptor = new PlatformStringCryptor();

  List<Board> boardMessages = List();
  Board board;
  final FirebaseDatabase database = FirebaseDatabase.instance;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  DatabaseReference databaseReference;

  String dropdownValue = 'Location';

//
  @override
  void initState() {
    _getLocation().then((value) {
      setState(() {
        userLocation = value;
      });
    });
    print("locationnn"+userLocation.toString());
    super.initState();
    PluginGooglePlacePicker.initialize(
      androidApiKey: "AIzaSyD_sXdHI8w9CLEw0mbYBm1HK5TteXIS84U"

    );

    //PluginGooglePlacePicker.showAutocomplete(bias: LocationBias(),countryCode: "961",mode: PlaceAutocompleteMode.MODE_OVERLAY,restriction: LocationRestriction(),typeFilter: TypeFilter.CITIES);
    //print(    PluginGooglePlacePicker.showAutocomplete(bias: LocationBias(),countryCode: "961",mode: PlaceAutocompleteMode.MODE_OVERLAY,restriction: LocationRestriction(),typeFilter: TypeFilter.CITIES).toString());
    board = Board("", "","","","","");
    databaseReference = database.reference().child("Lost Items");
    databaseReference.onChildAdded.listen(_onEntryAdded);
    databaseReference.onChildChanged.listen(_onEntryChanged);
  }

  File image;
  Future getImage() async {
    File picture = await ImagePicker.pickImage(
        source: ImageSource.gallery, maxWidth: 300.0, maxHeight: 500.0);
    setState(() {
      image = picture;

    });
  }

  Future getImageCam() async {
    File picture = await ImagePicker.pickImage(
        source: ImageSource.camera, maxWidth: 300.0, maxHeight: 500.0);
    setState(() {
      image = picture;

    });
  }
  final GlobalKey _menuKey = new GlobalKey();

  @override
  Widget build(BuildContext context) {
    isLogIn();
    return new Scaffold(
      backgroundColor:  Colors.teal,
      appBar: AppBar(
        backgroundColor: Color(0xFF009688),
        title: Text('New Post'),
      ),
      body: SingleChildScrollView(
        child: Stack(
          children: <Widget>[
            new Column(
              children: <Widget>[
                Flexible(
                  flex: 0,
                  child: Center(
                    child: Form(
                      key: formKey,
                      child: Flex(
                        direction: Axis.vertical,
                        children: <Widget>[

                          ListTile(
                            leading: Icon(Icons.subject, size: 40.0, color: Colors.white,),
                            title: TextFormField(
                              initialValue: "",
                              decoration: InputDecoration(
                                  enabledBorder: const UnderlineInputBorder(
                                      borderSide: const BorderSide(color: Colors.white)
                                  ),
                                  labelText: 'Subject',
                                  labelStyle: new TextStyle(color: Colors.white),
                                  fillColor: Colors.white
                              ),
                              onSaved: (val) => board.subject = val,
                              validator: (val) => val == "" ? val : null,
                            ),

                          ),

                          ListTile(
                            leading: Icon(Icons.message, size: 40.0,color: Colors.white),
                            title: TextFormField(
                              decoration: InputDecoration(
                                  enabledBorder: const UnderlineInputBorder(
                                      borderSide: const BorderSide(color: Colors.white)
                                  ),
                                  labelText: 'Description',
                                  labelStyle: new TextStyle(color: Colors.white),
                                  fillColor: Colors.white

                              ),
                              initialValue: "",
                              onSaved: (val) => board.body = val,
                              validator: (val) => val == "" ? val : null,
                            ),
                          ),

                          ListTile(
                            leading: Icon(Icons.pin_drop, size: 40.0,color: Colors.white),
                            title:
                              DropdownButton<String>(
                              value: dropdownValue,
                              onChanged: (String newValue) {
                                setState(() {
                                  dropdownValue = newValue;
                                });
                                },

                              items: <String>['Location', 'Beirut', 'Ashrafiye', 'Alley', 'Downtown', 'Saida', 'Dora', 'Jbeil']
                                  .map<DropdownMenuItem<String>>((String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),

                                );
                              }).toList(),
                            ),
                          ),
                          Divider(
                            height: 40.0,
                          ),

//              DropdownButton(
//                value: _selectedCountryCode,
//                items: _countrycodes
//                    .map((code) =>
//                new DropdownMenuItem(value: code, child: new Text(code)))
//                    .toList(),
//                onChanged: null,
//              ),

                          image == null
                          ? Text('No Image Uploaded', style: TextStyle(color: Colors.white),)
                          : Image.file(image),

//                          SizedBox(height: 8.0),
                          _buildButtons(context),
//                          RaisedButton(
//                            child: Text("Add Image From Gallery"),
//                            textColor: Colors.teal,
//                            color: Colors.white,
//                            onPressed: getImage,
//                          ),
//
//                          RaisedButton(
//                            child: Text("Add Image From Camera"),
//                            textColor: Colors.teal,
//                            color: Colors.white,
//                            onPressed: getImageCam,
//                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            )
          ],
        )
      )
      ,
      floatingActionButton: Align(
        child:  FloatingActionButton (
          child: Icon(Icons.add),
          backgroundColor: Colors.white,
          foregroundColor: Colors.teal,
          onPressed: () {
            handleSubmit();
          },
        ),
        alignment:Alignment(1, 1)
      ),
    );


  }

  void _onEntryAdded(Event event) {
    setState(() {
      boardMessages.add(Board.fromSnapshot(event.snapshot));
    });
  }

  Future handleSubmit() async {
    final FormState form = formKey.currentState;
    if (form.validate()) {
      if(image!=null) {
        final StorageReference firebaseStorageRef =
        FirebaseStorage.instance.ref().child(randomAlpha(5) + '.jpg');
        final StorageUploadTask task =
        firebaseStorageRef.putFile(image);


//      if( task.isComplete){

        StorageTaskSnapshot taskSnapshot = await task.onComplete;

        board.imgURL = await firebaseStorageRef.getDownloadURL() as String;
      }
      _getLocation().then((value) {
        setState(() {
          userLocation = value;
        });
      });
      print("locationnn"+userLocation.toString());
      form.save();
      form.reset();
      //save form data to the database
      FirebaseUser CurrentUser= await FirebaseAuth.instance.currentUser();

      board.userID=CurrentUser.uid;
      databaseReference.push().set(board.toJson());

      Navigator.pop(context);
//      }
    }
  }

  void _onEntryChanged(Event event) {
    var oldEntry = boardMessages.singleWhere((entry) {
      return entry.key == event.snapshot.key;
    });

    setState(() {
      boardMessages[boardMessages.indexOf(oldEntry)] =
          Board.fromSnapshot(event.snapshot);
    });
  }
  isLogIn() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String login= prefs.getString("login");
    print(login);

  }


  Widget _buildButtons(context)  {
    BuildContext context2;
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 100.0),
      child: Row(
        children: <Widget>[

          SizedBox (width: 10.0),

          Expanded (
            child: InkWell(
              onTap:()=> _uploadImage(context),
              child: Container(
                height: 40.0,
                decoration: BoxDecoration(
//                  border: Border.all(),
                  color: Colors.white,
                ),
                child: Center(
                  child: Padding(

                    padding: EdgeInsets.all(10.0),
                    child: Text(

                      "UPLOAD IMAGE",
                      style: TextStyle(color: Colors.teal,
                        fontWeight: FontWeight.w600,),
                    ),
                  ),
                ),
              ),
            ),
          ),

        ],
      ),
    );
  }

  Future _uploadImage(BuildContext context  ) async {

    switch(
    await showDialog(
        context: context,
        child: new SimpleDialog(
          title: new Text("Upload Image"),
          children: <Widget>[
            new SimpleDialogOption(
              child: new Text("Gallery"),
              onPressed: (){
                Navigator.pop(context, "Gallery");

                getImage();

              },
            ),
            new SimpleDialogOption(
              child: new Text("Camera"),
              onPressed: (){
                Navigator.pop(context, "Camera");
                getImageCam();

              },
            ),
          ],
        )
    )
    ) {
      case "Gallery":
        getImage();
        break;
      case "Camera":
        getImageCam();
        break;
    }

  }
  Future<Map<String, double>> _getLocation() async {
    var currentLocation = <String, double>{};
    try {
      currentLocation = await location.getLocation();
    } catch (e) {
      currentLocation = null;
    }
    return currentLocation;
  }
}




