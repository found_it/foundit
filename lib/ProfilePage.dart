import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'model/user.dart';


//class MyProfilePage extends StatefulWidget {
//  FirebaseUser user;
//  @override
//  ProfilePage createState() => new ProfilePage();
//}
 class  ProfilePage extends StatelessWidget  {
  @override
  Widget build(BuildContext context) {
    getData();
    return MaterialApp(
      title: "User Profile",
      debugShowCheckedModeBanner: false,
      home: UserProfilePage(),

    );

  }

}
//
String _fullName = "";
String _status = "";
String _img = "";

String _bio =
    "Hi, I am Living In Ashrafieh. I hope I will Find your Items. :)";
String _followers = "3.3";
String _posts = "24";
String _scores = "450";

 class UserProfilePage extends StatelessWidget {

  Widget _buildCoverImage(Size screenSize) {


    return Container(

      height: screenSize.height / 2.6,
      decoration: BoxDecoration(
        image: DecorationImage(
          image:AssetImage('assets/images/logo.png'),
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  Widget _buildProfileImage() {
    return Center(
      child: Container(
        width: 140.0,
        height: 140.0,
        decoration: BoxDecoration(
          image: DecorationImage(
            image:  AssetImage( _img),
            fit: BoxFit.cover,
          ),
          borderRadius: BorderRadius.circular(80.0),
          border: Border.all(
            color: Colors.white,
            width: 10.0,
          ),
        ),
      ),
    );
  }

  Widget _buildFullName() {
    TextStyle _nameTextStyle = TextStyle(
      fontFamily: 'Roboto',
      color: Colors.black,
      fontSize: 28.0,
      fontWeight: FontWeight.w700,
    );

    return Text(
      _fullName,
      style: _nameTextStyle,
    );
  }

  Widget _buildStatus(BuildContext context) {
    return Container(

      padding: EdgeInsets.symmetric(vertical: 4.0, horizontal: 6.0),
      decoration: BoxDecoration(
        color: Theme.of(context).scaffoldBackgroundColor,
        borderRadius: BorderRadius.circular(4.0),
      ),
      child: Text(

        _status,
        style: TextStyle(
          fontFamily: 'Spectral',
          color: Colors.black,
          fontSize: 20.0,
          fontWeight: FontWeight.w300,
        ),
      ),
    );
  }

  Widget _buildStatItem(String label, String count) {
    TextStyle _statLabelTextStyle = TextStyle(
      fontFamily: 'Roboto',
      color: Colors.black,
      fontSize: 16.0,
      fontWeight: FontWeight.w200,
    );

    TextStyle _statCountTextStyle = TextStyle(

      color: Colors.black54,
      fontSize: 24.0,
      fontWeight: FontWeight.bold,
    );

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          count,
          style: _statCountTextStyle,
        ),
        Text(
          label,
          style: _statLabelTextStyle,
        ),
      ],
    );
  }

  Widget _buildStatContainer() {
    return Container(
      height: 60.0,
      margin: EdgeInsets.only(top: 30.0),
      decoration: BoxDecoration(
        color: Color(0xFFEFF4F7),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          _buildStatItem("Rating", _followers),
          _buildStatItem("Posts", _posts),
          _buildStatItem("Points", _scores),
        ],
      ),
    );
  }

  Widget _buildBio(BuildContext context) {
    TextStyle bioTextStyle = TextStyle(
      fontFamily: 'Spectral',
      fontWeight: FontWeight.w400,//try changing weight to w500 if not thin
      fontStyle: FontStyle.italic,
      color: Color(0xFF799497),
      fontSize: 16.0,
    );

    return Container(
      color: Colors.teal,

      padding: EdgeInsets.all(8.0),
      child: Text(
        _bio,
        textAlign: TextAlign.center,
        style: TextStyle(fontStyle: FontStyle.italic,color: Colors.white,fontSize: 18.0) ,


      ),
    );
  }

  Widget _buildSeparator(Size screenSize) {
    return Container(
      width: screenSize.width / 1.6,
      height: 2.0,
      color: Colors.black54,
      margin: EdgeInsets.only(top: 4.0),
    );
  }

  Widget _buildGetInTouch(BuildContext context) {
    return Container(
      color: Colors.teal,
      padding: EdgeInsets.only(top: 8.0),
//      child: Text(
//        _status+", Account Details", //${_fullName.split(" ")[0]}
//        style: TextStyle(fontFamily: 'Roboto', fontSize: 16.0),
//      ),
    );
  }

  Widget _buildButtons(context)  {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
      child: Row(
        children: <Widget>[
          Expanded(
            child: InkWell(
              onTap: () => print("followed"),
              child: Container(
                height: 40.0,
                decoration: BoxDecoration(
                  border: Border.all(),
                  color: Color(0xFF404A5C),
                ),
                child: Center(
                  child: Text(
                    "EDIT INFO",
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
              ),
            ),
          ),
          SizedBox (width: 10.0),
          Expanded (
            child: InkWell(
              onTap:()=> _askUser(context),
              child: Container(
                height: 40.0,
                decoration: BoxDecoration(
                  border: Border.all(),
                  color: Color(0xFF404A5C),
                ),
                child: Center(
                  child: Padding(

                    padding: EdgeInsets.all(10.0),
                    child: Text(

                      "MANAGE ACCOUNT",
                      style: TextStyle(color: Colors.white,
                        fontWeight: FontWeight.w600,),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    getData();
    Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.teal,
      body: Stack(
        children: <Widget>[
          _buildCoverImage(screenSize),
          SafeArea(
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  SizedBox(height: screenSize.height / 6.4),
                  _buildProfileImage(),
                  _buildFullName(),
                  _buildStatus(context),
                  _buildStatContainer(),
                  _buildBio(context),
                  _buildSeparator(screenSize),
                  SizedBox(height: 10.0),
                  _buildGetInTouch(context),
                  SizedBox(height: 8.0),
                  _buildButtons(context),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
  Future _askUser(BuildContext context  ) async {

    switch(
    await showDialog(
        context: context,


        child: new SimpleDialog(
          title: new Text("Manage Account"),
          children: <Widget>[
            new SimpleDialogOption(
              child: new Text("Verify Email"),
              onPressed: (){
                if(!User.isEmailVerified) {
                  User.sendEmailVerification();
                  Navigator.pop(context, "Verify Email");
                  showToast("Email Sent!");

                }
                else
                  showToast(User.email+" Email is Verified!");


              },
            ),
            new SimpleDialogOption(
              child: new Text("Reset Password"),
              onPressed: (){
                Navigator.pop(context, "Reset Password");

                forgotpass(User.email);
                showToast("Email Sent!");

              },
            ),
            new SimpleDialogOption(
              child: new Text("Delete Account"),
              onPressed: (){
                Navigator.pop(context, "Delete Account");
                User.delete();
                saveValues();
                showToast("Account Deleted");
                

              },
            ),

          ],
        )
    )
    ) {
      case "Verify Email":


        break;
      case "Reset Password":
        forgotpass(User.email);
        break;
      case "Delete Account":
        User.delete();
        saveValues();
        showToast("Account Deleted");
        break;
    }
  }

  void forgotpass(email) async{

    await auth
        .sendPasswordResetEmail(email: email);

    }
  saveValues() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("login", "false");

  }

showToast(String data){
  Fluttertoast.showToast(
      msg: data,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.CENTER,
      timeInSecForIos: 1,
      backgroundColor: Colors.greenAccent,

      textColor: Colors.white,
      fontSize: 16.0
  );

}
}
//class ProfilePage extends State<MyProfilePage> {
//
//  @override
//  void initState() {
//    super.initState();
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    userData();
//    return new Scaffold(
//      backgroundColor: Colors.teal,
//      appBar: AppBar(
//        backgroundColor: Color(0xFF009688),
//
//        title: Text('My Profile'),
//      ),
//      body: Column(
//        children: <Widget>[
//          Flexible(
//            flex: 0,
//            child: Center(
//              child: Form(
//                child: Flex(
//                  direction: Axis.vertical,
//                  children: <Widget>[
//
//
//                  ],
//                ),
//              ),
//            ),
//          ),
//
//        ],
//      ),
//    );
//  }

//


FirebaseUser User;
FirebaseAuth auth = FirebaseAuth.instance;

getData() async{

  user u;
  SharedPreferences prefs = await SharedPreferences.getInstance();

  FirebaseUser CurrentUser= await FirebaseAuth.instance.currentUser();
  FirebaseDatabase database = FirebaseDatabase.instance;
  DatabaseReference databaseReference = database.reference().child("users");
  databaseReference.child(CurrentUser.uid).once().then((DataSnapshot snapshot) {
    print(snapshot.value.toString());
    _fullName=snapshot.value["fname"]+" "+snapshot.value["lasname"];
    _status=snapshot.value["email"];
    _img = snapshot.value["imgURL"];
  });




}

