import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:found_it/model/board.dart';
import 'package:found_it/Main Page/sign_in.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:firebase_database/firebase_database.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CardPost extends StatefulWidget {
  final String subject, body,  imgURL;


  CardPost({Key key, @required this.subject, @required this.body, @required this.imgURL}) : super(key: key);
//  CardPost(String subject, String body, String imgURL){
//    this.body=body;
//    this.subject=subject;
//    this.imgURL=imgURL;
//
//  }




  @override
  Create createState() => new Create();
}
class Create extends State<CardPost> {


  @override
  Widget build(BuildContext context) {
    Widget titleSelection = new Container(

      padding: EdgeInsets.all(10.0),
      child: new Row(
        children: <Widget>[
          new Expanded(
              child: new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[

                  new Container(
                    padding: const EdgeInsets.only(bottom: 10.0),
                    child: new Text(  "${widget.subject}",
                        style: TextStyle(
                            fontSize: 35.0,
                            color: Colors.white,
                            fontWeight: FontWeight.bold
                        )
                    ),
                  ),

                  new Container(
                    padding: const EdgeInsets.only(bottom: 10.0),
                    child: new Text( "${widget.body}",
                        style: TextStyle(
                        fontSize: 20.0,
                        color: Colors.white,
                      )
                    ),
                  ),

                  new Container(
                    padding: const EdgeInsets.only(bottom: 10.0),
                    child:
                    new Text( "Ashrafien",
                        style: TextStyle(
                        fontSize: 20.0,
                        color: Colors.white,
                        fontStyle: FontStyle.italic
                      )
                    ),
                  ),
                  new Container(
                    padding: const EdgeInsets.only(bottom: 10.0),
                    child:
                    RaisedButton(
                      child: Text("Request Contact Info"),
                      textColor: Colors.teal,
                      color: Colors.white,
                      onPressed: () {
                        Fluttertoast.showToast(
                            msg: "Soon!",
                            toastLength: Toast.LENGTH_SHORT,
                            gravity: ToastGravity.CENTER,
                            timeInSecForIos: 1,
                            backgroundColor: Colors.red,
                            textColor: Colors.white,
                            fontSize: 16.0
                        );
                      },
                    ),
                  ),
                ],
              )),

//          RaisedButton(
//            child: Text("Confirm Found"),
//            textColor: Colors.teal,
//            color: Colors.white,
//            onPressed: () {
//              Fluttertoast.showToast(
//                  msg: "Soon!",
//                  toastLength: Toast.LENGTH_SHORT,
//                  gravity: ToastGravity.CENTER,
//                  timeInSecForIos: 1,
//                  backgroundColor: Colors.red,
//                  textColor: Colors.white,
//                  fontSize: 16.0
//              );
//            },
//          ),
        ],
      ),
    );
        return new Scaffold(
          backgroundColor:  Colors.teal,
          appBar: AppBar(
            backgroundColor: Color(0xFF009688),
            title: Text('The Post'),
          ),
          body: new ListView(
            children: <Widget>[
              new Container(
                padding: const EdgeInsets.all(10.0),
                child:
                new Text( "Posted by - Rabih Kadi",
                    style: TextStyle(
                        fontSize: 30.0,
                        color: Colors.white,
                        fontWeight: FontWeight.bold
                    )
                ),
              ),
              new Image.network(
                "${widget.imgURL}",
                fit: BoxFit.cover,
              ),
              titleSelection
            ],
          )
        );
  }


}